### Deploy API
Run `npm run deploy:serverless` to deploy the API to AWS. The resources defined in *serverless.yml* will be automatically instantiated using CloudFormation. You should copy the URL of the returned endpoint to *API_URL* in *config.json*.

### Build template
Run `npm run build` to set the form action on the website template to *API_URL* from *config.json*.

### Deploy website
Run `npm run deploy:static` to deploy the website using the AWS CLI.

## Libraries used
- [Serverless Framework](https://serverless.com) for project structure and deployment.
- [PaperCSS Framework](https://github.com/papercss/papercss) for the frontend design.
- [jQuery](https://jquery.com) to simplify working with the DOM and making AJAX queries.

This project was created from the following guide: https://medium.freecodecamp.org/how-to-build-a-serverless-url-shortener-using-aws-lambda-and-s3-4fbdf70cbf5c

Explaination of serverless.yml:
  * service: name of the service 
  * provider: select where to deploy the project, can be either cloud or on-site and configurations
  * stage and region are variables that you enter in the config.json
  * iamRoleStatements: creating a service user with access to selected resources
  * functions: the API Gateway methods are defined here

  * resources: this is the cloudFormation section where you define and configure everything that you want to be created in the aws ecosystem
